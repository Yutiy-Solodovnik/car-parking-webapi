﻿using CoolParking.BL.Models;
using System.IO;

namespace CoolParking.WebAPI.Interfaces
{
    public interface ITransactionControllerService
    {
        public VehicleBalanceUpdater GetVehicleBalanceUpdater(Stream request);
    }
}
