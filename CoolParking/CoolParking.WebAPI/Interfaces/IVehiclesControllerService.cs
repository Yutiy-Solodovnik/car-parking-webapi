﻿using CoolParking.BL.Models;
using System.IO;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IVehiclesControllerService
    {
        public Vehicle GetVehicle(Stream request);
    }
}
