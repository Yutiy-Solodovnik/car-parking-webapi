﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Services
{
    public class VehiclesControllerService: IVehiclesControllerService
    {
        public Vehicle GetVehicle(Stream request)
        {
            Task<string> todoJson = new StreamReader(request).ReadToEndAsync();
            Vehicle newVehicle = JsonConvert.DeserializeObject<Vehicle>(todoJson.Result);
            return newVehicle;
        }
    }
}
