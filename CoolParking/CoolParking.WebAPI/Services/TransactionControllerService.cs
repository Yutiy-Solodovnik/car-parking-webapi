﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Services
{
    public class TransactionControllerService: ITransactionControllerService
    {
        public VehicleBalanceUpdater GetVehicleBalanceUpdater(Stream request)
        {
            Task<string> todoJson = new StreamReader(request).ReadToEndAsync();
            VehicleBalanceUpdater vehicleBalanceUpdater = JsonConvert.DeserializeObject<VehicleBalanceUpdater>(todoJson.Result);
            return vehicleBalanceUpdater;
        }
    }
}
