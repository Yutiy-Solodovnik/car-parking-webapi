﻿using CoolParking.BL.Services;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TransactionsController : ControllerBase
    {
        private ParkingService _parkingService;
        private ITransactionControllerService _transactionControllerService;

        public TransactionsController(ParkingService parkingService, ITransactionControllerService transactionControllerService)
        {
            _parkingService = parkingService;
            _transactionControllerService = transactionControllerService;
        }

        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionInfo>> GetLastParkingTransactions()
        {
            return Ok(_parkingService.GetLastParkingTransactions());
        }

        [HttpGet("all")]
        public ActionResult<string> ReadFromLog()
        {
            try
            {
                return Ok(_parkingService.ReadFromLog());
            }
            catch (Exception)
            {
                return NotFound("File not found");
            }
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> UpdateVehicle()
        {
            try
            {
                VehicleBalanceUpdater vehicleBalanceUpdater = _transactionControllerService.GetVehicleBalanceUpdater(Request.Body);
                Vehicle.ValidateNumber(vehicleBalanceUpdater.Id);
                if(Parking.FindById(vehicleBalanceUpdater.Id) == null)
                    return NotFound("No vehicle with this id");
                if (vehicleBalanceUpdater?.Sum <= 0)
                    return BadRequest("Invalid body");
                _parkingService.TopUpVehicle(vehicleBalanceUpdater.Id, vehicleBalanceUpdater.Sum);
                return Ok(Parking.FindById(vehicleBalanceUpdater.Id));
            }
            catch (ArgumentException)
            {
                return BadRequest("Invalid body");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
