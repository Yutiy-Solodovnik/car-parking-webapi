﻿using CoolParking.BL.Services;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class VehiclesController : ControllerBase
    {
        private ParkingService _parkingService;
        private IVehiclesControllerService _vehiclesControllerService;
        public VehiclesController(ParkingService parkingService, IVehiclesControllerService vehiclesControllerService)
        {
            _vehiclesControllerService = vehiclesControllerService;
            _parkingService = parkingService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> GetVehicles()
        {
            return Ok(_parkingService.GetVehicles());
        }

        [HttpPost]
        public ActionResult<Vehicle> AddVehicle()
        {
            try
            {
                Vehicle newVehicle = _vehiclesControllerService.GetVehicle(Request.Body);
                _parkingService.AddVehicle(newVehicle);
                return Created("Vehicle is created", newVehicle);
            }
            catch (ArgumentException)
            {
                return BadRequest("Invalid body");
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Parking is full");
            }   
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicleById(string id)
        {
            try
            {
                Vehicle.ValidateNumber(id);
                Vehicle findVehicle = Parking.FindById(id);
                if (findVehicle == null)
                    return NotFound("This vehicle is not exsist");
                return Ok(findVehicle);
            }
            catch (ArgumentException)
            {
                return BadRequest("Invalid id");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<string> DeleteVehicle(string id)
        {
            try
            {
                Vehicle.ValidateNumber(id);
                Vehicle vehicleToRemove = Parking.FindById(id);
                if(vehicleToRemove == null)
                    return NotFound("Vehicle not found");
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                return BadRequest("Invalid id");
            }
            catch (InvalidOperationException)
            {
                return BadRequest("This vehicle in the debt");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
