﻿using CoolParking.BL.Models;
using System.Net.Http;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using CoolParking.ConsoleApp;

namespace CoolParkng.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MyJsonConverter jc = new MyJsonConverter();
            using (HttpClient client = new HttpClient())
            {
                string stringVariant;
                client.BaseAddress = new Uri("https://localhost:44331/");

                Console.WriteLine("Hi! This is a parking simulator");
                do
                {
                    Console.WriteLine("Choose your variant:");
                    Console.WriteLine("1. Display the current balance of the Parking.\n2. Display the amount of free places.\n" +
                        "3. Display the number of free/occupied parking spaces.\n4. Display all Parking Transactions for the current period(before being logged).\n" +
                        "5. Print the transaction history(by reading data from the Transactions.log file).\n6. Display the list of vehicles are located on the Parking.\n" +
                        "7. Add vehicle to trhe Parking\n8. Remove a vehicle from the Parking.\n9. Top up the balance of a vehicle.\n10. Find by a number. \n0. Exit.\n");

                    Validate(out stringVariant, 0, 10);
                    switch (Byte.Parse(stringVariant))
                    {
                        case 1:
                            {
                                Console.WriteLine($"{GetHttpResponseMessageContent(client, "api/parking/balance")} - Parking balance");
                                break;
                            }
                        case 2:
                            {
                                Console.WriteLine($"{GetHttpResponseMessageContent(client, "api/parking/freePlaces")} - Parking free places");
                                break;
                            }
                        case 3:
                            {
                                Console.WriteLine($"{GetHttpResponseMessageContent(client, "api/parking/capacity")} - Parking capicity");
                                break;
                            }
                        case 4:
                            {
                                Task<HttpResponseMessage> response = client.GetAsync("api/transactions/last");
                                response.Wait();
                                if (response.Result.StatusCode != System.Net.HttpStatusCode.OK)
                                {
                                    WriteWrongMessage(GetResponseStatus(response));
                                }
                                else
                                {
                                    Task<string> message = response.Result.Content.ReadAsStringAsync();
                                    message.Wait();
                                    IEnumerable<TransactionInfo> lastTransactions = jc.GetObjectFromJson<IEnumerable<TransactionInfo>>(message);
                                    Console.WriteLine("All Parking Transactions for the current period:");
                                    foreach (TransactionInfo transaction in lastTransactions)
                                    {
                                        Console.WriteLine(transaction.ToString());
                                    }
                                }
                                break;
                            }
                        case 5:
                            {
                                Task<HttpResponseMessage> response = client.GetAsync("api/transactions/all");
                                response.Wait();
                                if (response.Result.StatusCode != System.Net.HttpStatusCode.OK)
                                {
                                    WriteWrongMessage(GetResponseStatus(response));
                                }
                                else
                                {
                                    Task<string> message = response.Result.Content.ReadAsStringAsync();
                                    message.Wait();
                                    string allTransactions = jc.GetObjectFromJson<string>(message);
                                    Console.WriteLine($"All transaction history: \n{allTransactions}");
                                }
                                break;
                            }
                        case 6:
                            {
                                try
                                {
                                    Console.WriteLine("All vehicles in the Parking:");
                                    Task<HttpResponseMessage> response = client.GetAsync("api/vehicles");
                                    response.Wait();
                                    Console.WriteLine(response.Result.StatusCode);
                                    Task<string> message = response.Result.Content.ReadAsStringAsync();
                                    message.Wait();
                                    IEnumerable<Vehicle> addedVehicle = jc.GetObjectFromJson<IEnumerable<Vehicle>>(message);
                                    foreach (Vehicle vehicle in addedVehicle)
                                    {
                                        Console.WriteLine(vehicle.ToString());
                                    }
                                }
                                catch (Exception ex)
                                {
                                    WriteWrongMessage(ex.Message);
                                }
                                break;
                            }
                        case 7:
                            {
                                try
                                {
                                    string id = default;
                                    VehicleType vehicleType = default;
                                    string balance = default;
                                    Console.WriteLine("Write a number. Write own number (write 1) or generate (write 2)");
                                    Validate(out stringVariant, 1, 2);
                                    switch (Byte.Parse(stringVariant))
                                    {
                                        case 1:
                                            Console.Write("Input the number of type ХХ-YYYY-XX (like DV-2345-KJ):");
                                            id = Console.ReadLine();
                                            break;
                                        case 2:
                                            id = Vehicle.GenerateRandomRegistrationPlateNumber();
                                            Console.WriteLine($"Number is {id}");
                                            break;
                                    }
                                    Console.WriteLine("Chose type of vehicle (1 - Passenger Car, 2 - Truck, 3 - Bus, 4 - Motorcycle.");
                                    Validate(out stringVariant, 1, 4);
                                    switch (Byte.Parse(stringVariant))
                                    {
                                        case 1:
                                            vehicleType = VehicleType.PassengerCar;
                                            Console.WriteLine("This vehicle is Passenger Car");
                                            break;
                                        case 2:
                                            vehicleType = VehicleType.Truck;
                                            Console.WriteLine("This vehicle is Truck");
                                            break;
                                        case 3:
                                            vehicleType = VehicleType.Bus;
                                            Console.WriteLine("This vehicle is Bus");
                                            break;
                                        case 4:
                                            vehicleType = VehicleType.Motorcycle;
                                            Console.WriteLine("This vehicle is Motorcycle");
                                            break;
                                    }
                                    Console.Write("Input vehicle balance sum: ");
                                    Validate(out balance, 1, Int32.MaxValue);
                                    Vehicle vehicle = new Vehicle(id, vehicleType, Decimal.Parse(balance));
                                    Task<HttpResponseMessage> response = client.PostAsync("api/vehicles", new StringContent(jc.ConvertToJson(vehicle)));
                                    response.Wait();
                                    if (response.Result.StatusCode != System.Net.HttpStatusCode.Created)
                                    {
                                        WriteWrongMessage(GetResponseStatus(response));
                                    }
                                    else
                                    {
                                        Task<string> message = response.Result.Content.ReadAsStringAsync();
                                        message.Wait();
                                        Vehicle addedVehicle = jc.GetObjectFromJson<Vehicle>(message);
                                        Console.WriteLine($"{addedVehicle} has been already added");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    WriteWrongMessage(ex.Message);
                                }
                                break;
                            }
                        case 8:
                            {
                                try
                                {
                                    string numberToRemove;
                                    Console.WriteLine("Write a number.");
                                    numberToRemove = Console.ReadLine();
                                    Task<HttpResponseMessage> response = client.DeleteAsync($"api/vehicles/{numberToRemove}");
                                    response.Wait();
                                    if (response.Result.StatusCode != System.Net.HttpStatusCode.NoContent)
                                    {
                                        WriteWrongMessage(GetResponseStatus(response));
                                    }
                                    else
                                    {
                                        Task<string> message = response.Result.Content.ReadAsStringAsync();
                                        message.Wait();
                                        Console.WriteLine($"Vehicle has been already deleted");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    WriteWrongMessage(ex.Message);
                                }
                                break;
                            }
                        case 9:
                            {
                                try
                                {
                                    string numberToAdd;
                                    string balanceToAdd;
                                    Console.WriteLine("Write a number.");
                                    numberToAdd = Console.ReadLine();
                                    Console.Write("Input vehicle balance sum: ");
                                    Validate(out balanceToAdd, 1, Int32.MaxValue);
                                    string jsonToUpVehicle = jc.ConvertToJson(new VehicleBalanceUpdater(numberToAdd, Decimal.Parse(balanceToAdd)));
                                    Task<HttpResponseMessage> response = client.PutAsync($"api/transactions/topUpVehicle", new StringContent(jsonToUpVehicle));
                                    response.Wait();
                                    if (response.Result.StatusCode != System.Net.HttpStatusCode.NoContent)
                                    {
                                        WriteWrongMessage(GetResponseStatus(response));
                                    }
                                    else
                                    {
                                        Console.WriteLine(response.Result.StatusCode);
                                        Task<string> message = response.Result.Content.ReadAsStringAsync();
                                        message.Wait();
                                        Vehicle updatedVehicle = jc.GetObjectFromJson<Vehicle>(message);
                                        Console.WriteLine($"{updatedVehicle} has been already updated");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    WriteWrongMessage(ex.Message);
                                }
                                break;
                            }
                        case 10:
                            {
                                string numberToFind;
                                try
                                {
                                    Console.WriteLine("Write a number.");
                                    numberToFind = Console.ReadLine();
                                    Task<HttpResponseMessage> response = client.GetAsync($"api/vehicles/{numberToFind}");
                                    response.Wait();
                                    if (response.Result.StatusCode != System.Net.HttpStatusCode.NoContent)
                                    {
                                        WriteWrongMessage(GetResponseStatus(response));
                                    }
                                    else
                                    {
                                        Task<string> message = response.Result.Content.ReadAsStringAsync();
                                        message.Wait();
                                        Vehicle foundVehicle = jc.GetObjectFromJson<Vehicle>(message);
                                        Console.WriteLine($"{foundVehicle} exists");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    WriteWrongMessage(ex.Message);
                                }
                                break;
                            }
                    }
                }
                while (Byte.Parse(stringVariant) != 0);
            }

            static void Validate(out string stringVariant, int from, int to)
            {
                do
                {
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.Green;
                    stringVariant = Console.ReadLine();
                    Console.ForegroundColor = ConsoleColor.White;
                    if (!IsRightInDiapason(stringVariant, from, to))
                    {
                        WriteWrongMessage("Error! Incorrect input");
                    }
                }
                while (!IsRightInDiapason(stringVariant, from, to));
            }


            static bool IsRightInDiapason(string stringVariant, int from, int to)
            {
                int variant;

                if (Int32.TryParse(stringVariant, out variant))
                {
                    if (variant >= from && variant <= to)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }

            static string GetHttpResponseMessageContent(HttpClient client, string path)
            {
                Task<HttpResponseMessage> response = client.GetAsync(path);
                response.Wait();
                Task<string> message = response.Result.Content.ReadAsStringAsync();
                message.Wait();
                return message.Result;
            }

            static string GetResponseStatus(Task<HttpResponseMessage> response)
            {
                Task<string> message = response.Result.Content.ReadAsStringAsync();
                message.Wait();
                return $"{response.Result.StatusCode}: {message.Result}";
            }

            static void WriteWrongMessage(string message)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
