﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;

namespace CoolParking.ConsoleApp
{
    public class MyJsonConverter
    {
        public string ConvertToJson<T>(T objectToConvert)
        {
            return JsonConvert.SerializeObject(objectToConvert);
        }

        public T GetObjectFromJson<T>(Task<string> json)
        {
            return JsonConvert.DeserializeObject<T>(json.Result);
        }
    }
}
