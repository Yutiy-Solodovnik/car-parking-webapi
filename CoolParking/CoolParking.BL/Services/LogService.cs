﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService( string path)
        {
            LogPath = path;
        }
        public string LogPath { get; }

        public string Read()
        {
            string logInfo = null;
            try
            {
                if (!IsExist())
                {
                    throw new IOException();
                }
                using (StreamReader sr = new StreamReader(new FileStream(LogPath, FileMode.Open, FileAccess.Read)))
                {
                    logInfo = sr.ReadToEnd();
                }
            }
            catch
            {
                throw new IOException("Can not connect to the file");
            }
            return logInfo;
        }

        public void Write(string logInfo)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(new FileStream(LogPath, FileMode.Append, FileAccess.Write)))
                {
                    sw.WriteLine(logInfo);
                }
            }
            catch
            {
                throw new IOException("Can not connect to the file");
            }
        }

        private bool IsExist()
        {
            return File.Exists(LogPath);
        }
    }
}