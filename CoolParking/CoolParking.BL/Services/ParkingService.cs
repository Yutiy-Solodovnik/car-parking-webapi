﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        Parking _parking;
        private readonly object _locker;
        private List<TransactionInfo> _transactions = new List<TransactionInfo>();
        public delegate void ParkingHandler();
        public event ParkingHandler moneyIsDrawn;


        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.Instance;
            _locker = new object();
            WithdrawTimer = withdrawTimer;
            LogTimer = logTimer;
            LogService = logService;
            WithdrawTimer.Elapsed += WithdrawMoney;
            LogTimer.Elapsed += LogTransactions;
            WithdrawTimer.Start();
            LogTimer.Start();
        }

        public ITimerService WithdrawTimer { get; set; }
        public ITimerService LogTimer { get; set; }
        public ILogService LogService { get; set; }

        private void WithdrawMoney(object sender, ElapsedEventArgs e)
        {
            foreach (Vehicle vehicle in _parking.GetVehicles())
            {
                decimal withdrawnMoney;
                if (vehicle.Balance > 0)
                {
                    if ((vehicle.Balance - Settings.Tariffs[vehicle.VehicleType]) > 0)
                    {
                        withdrawnMoney = Settings.Tariffs[vehicle.VehicleType];
                        _parking.Balance += withdrawnMoney;
                    }
                    else
                    {
                        decimal rest = Settings.Tariffs[vehicle.VehicleType] - vehicle.Balance;
                        withdrawnMoney = vehicle.Balance + rest * Settings.PenaltyRate;
                        _parking.Balance += vehicle.Balance;
                    }
                }
                else
                {
                    withdrawnMoney = Settings.Tariffs[vehicle.VehicleType] * Settings.PenaltyRate;
                }
                vehicle.Balance -= withdrawnMoney;
                lock (_locker)
                {
                    _transactions.Add(new TransactionInfo(DateTime.Now, vehicle.Id, withdrawnMoney, vehicle.Balance));
                }
            }
            moneyIsDrawn?.Invoke();
        }

        private void LogTransactions(object sender, ElapsedEventArgs e)
        {
            lock (_locker)
            {
                foreach (TransactionInfo transaction in _transactions)
                {
                    LogService.Write(transaction.ToString());
                }
                _transactions.Clear();
            }
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() <= 0)
            {
                throw new ArgumentException("Parking is full");
            }
            if (vehicle.VehicleType < 0 || (int)vehicle.VehicleType > 3)
                throw new ArgumentException("Invalid type");
            CheckVehicle(vehicle);
            _parking.AddVehicle(vehicle);
        }

        public decimal GetLastEarnedMoney()
        {
            decimal sum = 0;
            foreach (TransactionInfo transaction in _transactions)
            {
                sum += transaction.WithdrawnMoney;
            }
            return sum;
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.Capicity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - _parking.GetOccupiedPlaces();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.GetVehicles());
        }

        public string ReadFromLog()
        {
            lock (_locker)
            {
                return LogService.Read();
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            try
            {
                Vehicle searchedVehicle = _parking.GetVehicles().Where(s => s.Id == vehicleId).FirstOrDefault();
                if (searchedVehicle == null || searchedVehicle.Balance < 0)
                    throw new InvalidOperationException("This vehicle in the debt");
                else
                    _parking.GetVehicles().Remove(searchedVehicle);
            }
            catch
            {
                throw new ArgumentException("Vehicle with this number is not found");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            try
            {
                Vehicle searchedVehicle = Parking.FindById(vehicleId);
                if (searchedVehicle == null)
                    throw new ArgumentException("Is not find a number");
                if (sum < 0)
                    throw new ArgumentException("Negative sum");
                TakeFine(searchedVehicle, sum);
                searchedVehicle.ToUp(sum, searchedVehicle.Id);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Is not find a number");
            }
        }

        public static void CheckVehicle(Vehicle vehicle)
        {
            Vehicle.ValidateNumber(vehicle.Id);
            if (Parking.IsNumberExist(vehicle.Id))
                throw new ArgumentException("This number is exist");
            if (vehicle.Balance <= 0)
                throw new ArgumentException("Balance is negative");
        }

        private void TakeFine(Vehicle vehicle, decimal sum)
        {
            if (vehicle.Balance < 0)
            {
                decimal diff = vehicle.Balance + sum;
                _parking.Balance += (diff > 0) ? Math.Abs(vehicle.Balance) : sum;
            }
        }
        private bool IsFileExist(string path)
        {
            return File.Exists(path);
        }
        private void DeleteFile()
        {
            if (IsFileExist(LogService.LogPath))
                File.Delete(LogService.LogPath);
        }
        public void Dispose()
        {
            WithdrawTimer.Stop();
            WithdrawTimer.Dispose();
            LogTimer.Stop();
            LogTimer.Dispose();
            DeleteFile();
            _parking.Clear();
        }
    }
}