﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static Regex _regex = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$");
        private static Random _rand = new Random();

        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            ValidateNumber(id);
            Balance = balance;
            VehicleType = vehicleType;
            Id = id;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            char[] reegistrationPlate = new char[10];

            reegistrationPlate[2] = '-';
            reegistrationPlate[7] = '-';

            for (int i = 0; i < 2; i++)
            {
                reegistrationPlate[i] = GenerateCharLetter();
            }

            for (int i = 3; i < 7; i++)
            {
                reegistrationPlate[i] = GenerateCharNumber();
            }

            for (int i = 8; i < reegistrationPlate.Length; i++)
            {
                reegistrationPlate[i] = GenerateCharLetter();
            }

            return new string(reegistrationPlate);
        }

        static char GenerateCharLetter()
        {
            return (char)_rand.Next(0x0041, 0x005A);
        }

        static char GenerateCharNumber()
        {
            return (char)_rand.Next(0x0030, 0x0039);
        }

        public void ToUp(decimal sum, string id)
        {
            Balance += sum;
        }

        public static void ValidateNumber(string number)
        {
            if (!_regex.IsMatch(number))
                throw new ArgumentException("Invalid number");
        }

        public override string ToString()
        {
            return $"{VehicleType} with number: {Id} and balance: {Balance}";
        }
    }
}
