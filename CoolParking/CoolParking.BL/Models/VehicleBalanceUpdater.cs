﻿namespace CoolParking.BL.Models
{
    public class VehicleBalanceUpdater
    {
        public VehicleBalanceUpdater(string id, decimal sum)
        {
            Id = id;
            Sum = sum;
        }

        public string Id { get; }
        public decimal Sum { get; }
    }
}
