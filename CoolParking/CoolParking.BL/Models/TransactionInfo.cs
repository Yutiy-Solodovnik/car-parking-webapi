﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public DateTime TransactionTime { get; }
        public string VehiclePlateNumber { get; }
        public decimal WithdrawnMoney { get; }
        public decimal Sum { get; }
        public TransactionInfo(DateTime transactionTime, string vehiclePlateNumber, decimal withdrawnMoney, decimal sum)
        {
            TransactionTime = transactionTime;
            VehiclePlateNumber = vehiclePlateNumber;
            WithdrawnMoney = sum;
            Sum = withdrawnMoney;
        }

        public static string FindByRegex(string source, string pattern)
        {
            string res = null;
            foreach (Match match in Regex.Matches(source, pattern))
                res = match.Value;
            return res;
        }

        public override string ToString()
        {
            return $"[{TransactionTime.ToString("MM/dd/yyyy HH:mm")}] Vehicle with number [{VehiclePlateNumber}] " +
                $"and withdrawn money [{WithdrawnMoney}] with sum [{Sum}]";
        }
    }
}